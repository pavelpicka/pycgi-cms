"""
Run server for local debug.
"""

from http import server


handler = server.CGIHTTPRequestHandler
handler.cgi_directories.insert(0, '/')
print(handler.cgi_directories)
s = server.HTTPServer(('localhost', 9000), handler)
s.serve_forever()
