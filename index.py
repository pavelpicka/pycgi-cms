#!/usr/bin/env python3

import cgi
import os
try:
    from yaml import load, BaseLoader
    from jinja2 import Template
    from markdown2 import markdown
except ImportError:
    print("Content-type: text/html\r\n\r\n")
    print("Missing some dependencies<br>")
    print("Make sure you got PyYAML, Jinja2 and Markdown2 installed.")

# DEBUG, comment or remove if used
# import cgitb
# cgitb.enable()


def load_config():
    """Load basic config or return some defaults."""
    try:
        with open("config/cfg.yaml", 'r') as cfg:
            return load(cfg, Loader=BaseLoader)
    except OSError:
        return {
            "site_name": "PyCGI CMS Site",
            "language": "en",
            "theme": "default",
            "author": "PyCGI CMS",
            "footer": "(c) 2019 PyCGI CMS",
            "template": "default"
        }


def get_content(folder_name):
    """Get content in content folder ends with '.md' or blog style."""
    content_list = {"pages": [], "blogs": []}
    if os.path.isdir(folder_name):
        for obj in os.listdir(folder_name):
            if os.path.isfile(os.path.join(folder_name, obj)) and obj.endswith(".md"):
                if "404.md" in obj:
                    # doesn't have to be in menu
                    continue
                else:
                    content_list['pages'].append(obj[:-3])
            elif os.path.isdir(os.path.join(folder_name, obj)):
                content_list['blogs'].append(obj)
    return content_list


def parse_meta(filename, directory=False):
    """Parse metadata from content file."""
    if directory:
        name = "content/" + directory + "/" + filename + ".md"
    else:
        name = "content/" + filename + ".md"
    with open(name, 'r') as f:
        meta = ""
        meta_count = 0
        for line in f:
            if meta_count < 2:
                if line.startswith("---"):
                    meta_count += 1
                    continue
                meta += line
    meta = load(meta, Loader=BaseLoader)
    if not isinstance(meta, dict):
        meta = {}
    return meta


def parse_content(filename, directory=False):
    """Parse content from content file."""
    if directory:
        name = "content/" + directory + "/" + filename + ".md"
    else:
        name = "content/" + filename + ".md"
    with open(name, 'r') as f:
        content = ""
        meta_count = 0
        for line in f:
            if meta_count < 2:
                if line.startswith("---"):
                    meta_count += 1
                    continue
                continue
            else:
                content += line
    return markdown(content, extras=["fenced-code-blocks"])


def parse_file(filename, directory=False):
    """Return metadata and content separately."""
    return (
        parse_meta(filename, directory),
        parse_content(filename, directory)
        )


def render_page(page, blog=False, article=False, directory=False):
    """Render web page from content file."""
    template_file = "themes/" + cfg['theme'] + \
        "/" + cfg['template'] + ".j2"
    with open(template_file, 'r') as t:
        template = Template(t.read())

    if blog:
        articles = get_content("content/" + page)['pages']
        cfg['articles'] = []
        for post in articles:
            try:
                desc = parse_meta(post, page)['description']
            except KeyError:
                desc = "No description."
            cfg['articles'].append({
                "title": post,
                "desc": desc
            })
        cfg['title'] = page
    elif article and directory:
        meta, content = parse_file(page, directory)
        for key, value in meta.items():
            cfg[key] = value
        cfg['content'] = content
    else:
        meta, content = parse_file(page)
        for key, value in meta.items():
            cfg[key] = value
        cfg['content'] = content
    print(template.render(**cfg))


# check for content
content_list = get_content("content")
menu_list = content_list['pages'] + content_list['blogs']

# have home always as first
if "home" in menu_list:
    menu_list.remove("home")
    menu_list.insert(0, "home")

# get config and update it
cfg = load_config()
cfg['menu_list'] = menu_list

# get / post data
data = cgi.FieldStorage()
page = data.getvalue('p')
article = data.getvalue('a')
directory = data.getvalue('d')

# simple header
print("Content-type: text/html\r\n\r\n")

# body
if page in content_list['pages']:
    render_page(page)
elif page in content_list['blogs']:
    render_page(page, blog=True)
elif article and directory:
    if (directory in content_list["blogs"] and
            article in get_content("content/" + directory)["pages"]):
        render_page(article, directory=directory, article=True)
    else:
        render_page("404")
else:
    render_page("home")

