PyCGI CMS
=========

Really simple python 3 web content management system using CGI.
Follows KISS (Keep It Simple, Stupid!)

Homepage: [PyCGI CMS](https://gitlab.com/pavelpicka/pycgi-cms)

Motivation
----------

To have own simple and small CMS for my webpage.

Requirements
------------

Apache server

- PyCGI CMS in document root (any sub folder)
  - Because absolute paths is used
- CGI mod enabled
- rewrite mod enabled
  - for better security; deny access for configuration etc.
- allow `.htaccess` file
  - to can use rewrite and CGI mod

Python 3 modules

- Markdown2
- PyYAML
- Jinja2

Content Hierarchy
----------------

All content is inside 'content' directory.
File endings with `.md` are showed as pages.
Directories are shown as simple blog type pages with
list of underlying `.md` files.

Allowed are only one level of folder structure.

### Content Format

Each file must contain basic metadata starts and ends with `---`.
Metadata should contain at least `title` and `description`.

After that content itself written in markdown.

Simple example is `404.md` in content directory.

### Example

```
<root>
|
|- home.md  (required - first page to show)
|
`- blog [directory]  (list of files underneath)
 |
 `- post1  (page with content)
```

Settings
--------

Located in `config/cfg.yaml` and commented for
easy use.

Testing
-------

Use `run.py` to run local server with cgi support.

Issues
------

#### Internal error 500 on CentOS 7
CentOS 7 python 3 installation missing `python3` executable.
To fix edit `index.py` and add right `python3` executable.
E.g. `#!/usr/bin/env python3.6`
